﻿using System.Collections;
using System.Threading.Tasks;

namespace Revy.Framework
{
    public class FCoroutineAgent : FComponentSingleton<FCoroutineAgent>
    {
        public override Task Initialize()
        {
            CanInvokeFixedTick = false;
            CanInvokeLateTick = false;
            CanInvokeTick = false;

            FPersistent.Persist(this, PersistentSubCategories.UTILITIES);

            return base.Initialize();
        }
    }
}