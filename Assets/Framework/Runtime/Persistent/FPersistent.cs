﻿namespace Revy.Framework
{
    public class FPersistent : FComponent
    {
        protected override void Awake()
        {
            DontDestroyOnLoad(gameObject);
            MFramework.Register(this);
        }

        public override void PreInitialization()
        {
            CanInvokeTick = false;
            CanInvokeLateTick = false;
            CanInvokeFixedTick = false;
        }

        /// <summary>
        /// Persist component's game object and make it child of game object named 'parentName'.
        /// </summary>
        /// <param name="component"></param>
        /// <param name="parentName"></param>
        public static void Persist(FComponent component, string parentName)
        {
            //Find parent game object
            UnityEngine.Transform parentTransform = MFramework.PersistentGameObject.transform.Find(parentName);

            if (parentTransform == null)
            {
                parentTransform = new UnityEngine.GameObject(parentName).transform;

                parentTransform.SetParent(MFramework.PersistentGameObject.transform);
            }

            component.transform.SetParent(parentTransform);
        }

        /// <summary>
        /// Persist component's game object and make it child of game object named 'parentName'.
        /// </summary>
        /// <param name="component"></param>
        /// <param name="parentName"></param>
        public static void Persist(UnityEngine.GameObject gameObject, string parentName)
        {
            //Find parent game object
            UnityEngine.Transform parentTransform = MFramework.PersistentGameObject.transform.Find(parentName);

            if (parentTransform == null)
            {
                parentTransform = new UnityEngine.GameObject(parentName).transform;

                parentTransform.SetParent(MFramework.PersistentGameObject.transform);
            }

            gameObject.transform.SetParent(parentTransform);
        }

        /// <summary>
        /// Persist component's game object and make it child of game object named 'parentName'.
        /// </summary>
        /// <param name="component"></param>
        /// <param name="parentName"></param>
        public static void Persist(UnityEngine.Component gameObject, string parentName)
        {
            //Find parent game object
            UnityEngine.Transform parentTransform = MFramework.PersistentGameObject.transform.Find(parentName);

            if (parentTransform == null)
            {
                parentTransform = new UnityEngine.GameObject(parentName).transform;

                parentTransform.SetParent(MFramework.PersistentGameObject.transform);
            }

            gameObject.transform.SetParent(parentTransform);
        }
    }
}