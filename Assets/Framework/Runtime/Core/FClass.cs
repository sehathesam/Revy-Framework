﻿using System.Threading.Tasks;

namespace Revy.Framework
{
    /// <summary>
    /// FClass is the base class from which every framework class(not component) script derives.
    /// This class has all framework functionalities but could not attach to a game object.
    /// </summary>
    public abstract class FClass : IInitializable, IPauseable, ICanInvokeTick, ICanInvokeLateTick, ICanInvokeFixedTick
    {
        #region Fields
        private bool _canInvokeTick = false;
        private bool _canInvokeLateTick = false;
        private bool _canInvokeFixedTick = false;
        #endregion

        #region Properties
        public int ExecutionOrder { get; protected set; }

        public bool IsInitializing { get; protected set; }

        public bool HasInitialized { get; protected set; }

        public bool HasShutdown { get; set; }

        public bool CanInvokeTick
        {
            get { return _canInvokeTick; }
            set
            {
                if (value)
                    MFramework.AddtoTickList(this);
                else
                    MFramework.RemoveFromTickList(this);
            }
        }

        public bool CanInvokeLateTick
        {
            get { return _canInvokeLateTick; }
            set
            {
                if (value)
                    MFramework.AddToLateTickList(this);
                else
                    MFramework.RemoveFromLateTickList(this);
            }
        }

        public bool CanInvokeFixedTick
        {
            get { return _canInvokeFixedTick; }
            set
            {
                if (value)
                    MFramework.AddToFixedTickList(this);
                else
                    MFramework.RemoveFromFixedTickList(this);
            }
        }
        #endregion

        #region Constructor & Destructor
        public FClass()
        {
            CanInvokeTick = false;
            CanInvokeLateTick = false;
            CanInvokeFixedTick = false;
            MFramework.Register(this);
        }

        /// <summary>
        /// This method will invoke when Destroy() or DestroyImmediate has invoked for scriptable object.
        /// </summary>
        ~FClass()
        {
            MFramework.UnRegister(this);
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Unregister this instance form Framework.
        /// </summary>
        public void Unregister()
        {
            MFramework.UnRegister(this);
        }
        #endregion Public Methods

        #region IFBase's Methods
        public virtual void PreInitialization() { }

        public virtual Task Initialize() { return Task.CompletedTask; }

        //public virtual Task LoadResources() { return Task.CompletedTask; }

        public virtual Task BeginPlay() { return Task.CompletedTask; }

        public virtual Task Shutdown() { return Task.CompletedTask; }

        public virtual void OnResume() { }

        public virtual void OnPause() { }

        public virtual void Tick() { }

        public virtual void LateTick() { }

        public virtual void FixedTick() { }

        public int CompareTo(IInitializable other)
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
