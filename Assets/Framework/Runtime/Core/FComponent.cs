﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEngine;

namespace Revy.Framework
{
    /// <summary>
    /// FComponent is the base class from which every framework component(Attachable to a game object) script derives.
    /// </summary>
    public abstract class FComponent : MonoBehaviour, IInitializable, IPauseable, ICanInvokeTick, ICanInvokeLateTick, ICanInvokeFixedTick
    {
        #region Fields
        [ReadOnly, SerializeField] private int _executionOrder = 100;
        private bool _canInvokeTick = false;
        private bool _canInvokeLateTick = false;
        private bool _canInvokeFixedTick = false;
        private bool _canInvokeTickCache = false;
        private bool _canInvokeLateTickCache = false;
        private bool _canInvokeFixedTickCache = false;
        private bool _canChangeTickCacheValues = true;
        #endregion

        #region Properties

        public bool IsInitializing { get; private set; }

        public bool HasInitialized { get; private set; }

        public bool HasShutdown { get; private set; }

        public int ExecutionOrder
        {
            get { return _executionOrder; }
            protected set { _executionOrder = value; }
        }

        public bool CanInvokeTick
        {
            get { return _canInvokeTick; }
            set
            {
                if (value) MFramework.AddtoTickList(this);
                else MFramework.RemoveFromTickList(this);

                if (!enabled && _canChangeTickCacheValues)
                {
                    _canInvokeTickCache = value;
                    return;
                }

                if (!_canChangeTickCacheValues) return;

                _canInvokeTick = _canInvokeTickCache = value;
            }
        }

        public bool CanInvokeLateTick
        {
            get { return _canInvokeLateTick; }
            set
            {
                if (value) MFramework.AddToLateTickList(this);
                else MFramework.RemoveFromLateTickList(this);

                if (!enabled && _canChangeTickCacheValues)
                {
                    _canInvokeLateTickCache = value;
                    return;
                }

                if (!_canChangeTickCacheValues) return;

                _canInvokeLateTick = _canInvokeLateTickCache = value;
            }
        }

        public bool CanInvokeFixedTick
        {
            get { return _canInvokeFixedTick; }
            set
            {
                if (value) MFramework.AddToFixedTickList(this);
                else MFramework.RemoveFromFixedTickList(this);

                if (!enabled && _canChangeTickCacheValues)
                {
                    _canInvokeFixedTickCache = value;

                    return;
                }

                if (!_canChangeTickCacheValues) return;

                _canInvokeFixedTick = _canInvokeFixedTickCache = value;
            }
        }

        [CInject]
        protected static ResourceManagement.IResourceManagement ResourceManagement { get; private set; }
        #endregion

        #region Monos

        protected virtual void Awake()
        {
            IsInitializing = false;
            HasInitialized = false;
            HasShutdown = false;
            MFramework.Register(this);
            TransientThisGameObject();
        }

        /// <summary>
        /// Make this game object child of Transient game object.
        /// </summary>
        private void TransientThisGameObject()
        {
            var transientGameObject = MFramework.TransientGameObject;

            if (transform.parent != null) return;

            if (transientGameObject != null)
                gameObject.transform.SetParent(transientGameObject.transform);
#if LOG_WARNING

            else
                Debug.LogWarningFormat("Can not transient this ({0}) object because 'MFrameworkManager.TransientGameObject' is null.", gameObject.name);
#endif
        }

        protected virtual void Start()
        {
        }

        protected virtual void OnEnable()
        {
            CanInvokeFixedTick = _canInvokeFixedTickCache;
            CanInvokeLateTick = _canInvokeLateTickCache;
            CanInvokeTick = _canInvokeTickCache;
        }

        protected virtual void OnDisable()
        {
            /*
             * In CanInvokeX we check that if property has called when components is disabled, we will cache the desire value and use it when component become enable.
             * Note that if component was not enable, we won't change the original _canInvokeX fields.
             * Also OnDisable method will rise after enabled become false, so it has not effect on the original _canInvokeX fields.
             * So we need to set them manually.
            */

            _canChangeTickCacheValues = false;

            CanInvokeTick = _canInvokeTick = false;
            CanInvokeLateTick = _canInvokeLateTick = false;
            CanInvokeFixedTick = _canInvokeFixedTick = false;

            _canChangeTickCacheValues = true;
        }

        /// <summary>
        /// Note that iOS applications are usually suspended and do not quit. 
        /// You should tick "Exit on Suspend" in Player settings for iOS builds to cause the game to quit and not suspend, 
        /// otherwise you may not see this call. If "Exit on Suspend" is not ticked then you will see calls to OnApplicationPause instead.
        /// </summary>
        protected virtual void OnApplicationQuit()
        {
            //We do this here because at OnDestroy() it is possible that persistent game object is deactivate
            // and we need Invoke FComponent.Shutown() with StartCoroutin
            MFramework.UnRegister(this);
        }

        /// <summary>
        /// Use this method with care inside child objects.
        /// Always invoke base method.
        /// </summary>
        protected virtual void OnDestroy()
        {
            if (!HasShutdown)
                MFramework.UnRegister(this);
        }

        #endregion

        #region Class Interface

        public virtual void PreInitialization() { }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual Task BeginPlay() { return Task.CompletedTask; }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual Task Initialize() { return Task.CompletedTask; }

        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        //public virtual Task LoadResources() { return Task.CompletedTask; }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual Task Shutdown() { return Task.CompletedTask; }

        public virtual void OnResume() { }

        public virtual void OnPause() { }

        public virtual void Tick() { }

        public virtual void LateTick() { }

        public virtual void FixedTick() { }

        /// <summary>
        /// Use as parameter for StartCoroutine() method.
        /// e.g StartCoroutine( CoroutineWithCallback(method with IEnumerator return type, finish callback ) )
        /// </summary>
        /// <param gameObjectName="routine"></param>
        /// <param gameObjectName="callback"></param>
        /// <returns></returns>
        public IEnumerator CoroutineWithCallback(IEnumerator routine, Action callback)
        {
            yield return StartCoroutine(routine);
            callback();
        }

        /// <summary>
        /// Invoke callback when coroutine finished.
        /// </summary>
        /// <param gameObjectName="routine"></param>
        /// <param gameObjectName="callback"></param>
        /// <returns></returns>
        public IEnumerator CoroutineWithCallback(YieldInstruction yieldInstuct, Action callback)
        {
            yield return yieldInstuct;
            callback();
        }

        /// <summary>
        /// Wraps task functionality into the coroutine scheme. 
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public IEnumerator TaskToCoroutineAsync(Task task)
        {
            yield return new WaitUntil(() => task.IsCompleted);
        }

        /// <summary>
        /// Wraps task functionality into the coroutine scheme. 
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public IEnumerator TaskToCoroutineAsync<T>(Task<T> task)
        {
            yield return new WaitUntil(() => task.IsCompleted);
        }

        /// <summary>
        /// Wraps coroutine functionality into the async/task scheme. 
        /// </summary>
        /// <param name="coroutine"></param>
        /// <returns></returns>
        public async Task CoroutineToTaskAsync(IEnumerator coroutine)
        {
            TaskCompletionSource<System.Object> tcs = new TaskCompletionSource<object>();
            StartCoroutine(CoroutineWithCallback(coroutine, () => tcs.TrySetResult(null)));
            await tcs.Task;
        }

        /// <summary>
        /// Wraps coroutine functionality into the async/task scheme. 
        /// </summary>
        /// <param name="coroutine"></param>
        /// <returns></returns>
        public async Task CoroutineToTaskAsync(YieldInstruction yieldInstruct)
        {
            TaskCompletionSource<System.Object> tcs = new TaskCompletionSource<object>();
            StartCoroutine(CoroutineWithCallback(yieldInstruct, () => tcs.TrySetResult(null)));
            await tcs.Task;
        }

        #endregion

        #region Implement IComparable interface

        public int CompareTo(IInitializable other)
        {
            if (ExecutionOrder > other.ExecutionOrder) return 1;
            if (ExecutionOrder < other.ExecutionOrder) return -1;
            return 0;
        }

        #endregion

        #region Protected

        /// <summary>
        /// Halt any async operation.
        /// </summary>
        /// <returns></returns>
        protected async Task HaltProccess()
        {
            await CoroutineToTaskAsync(new WaitUntil(() => false));
        }

        #endregion

        #region StaticMethods

        /// <summary>
        /// Create a game object then add TBehaviour component to it and make it transient.
        /// </summary>
        /// <typeparam name="TComponent">Inherited from FComponent</typeparam>
        /// <param name="gameObjectName">Instantiated object's name. By default it set to TComponenet full name.</param>
        /// <returns>The instantiated component.</returns>
        public static TComponent InstantiateTransient<TComponent>(string gameObjectName) where TComponent : FComponent
        {
            if (string.IsNullOrEmpty(gameObjectName)) gameObjectName = typeof(TComponent).ToString();

            UnityEngine.GameObject transientChildObject = new UnityEngine.GameObject(gameObjectName, typeof(TComponent));

            transientChildObject.transform.SetParent(MFramework.TransientGameObject.transform);

            return transientChildObject.GetComponent<TComponent>();
        }


        public static TComponent InstantiateTransient<TComponent>(TComponent original) where TComponent : Component
        {
            TComponent transientChildObject = Instantiate(original);

            transientChildObject.transform.SetParent(MFramework.TransientGameObject.transform);

            return transientChildObject;
        }


        /// <summary>
        /// Clone an object from original object and make it transient.
        /// </summary>
        /// <returns>The instantiated GameObject.</returns>
        public static GameObject InstantiateTransient(FComponent original)
        {
            if (original == null) return null;
            return Instantiate(original.gameObject, MFramework.TransientGameObject.transform);
        }

        /// <summary>
        /// Clone an object from original object and make it transient.
        /// </summary>
        /// <returns>The instantiated GameObject.</returns>
        public static GameObject InstantiateTransient(GameObject original)
        {
            if (original == null) return null;
            return Instantiate(original, MFramework.TransientGameObject.transform);
        }

        /// <summary>
        /// Instantiate a game object and attach component to it.
        /// Component's game object can have a parent game object if  'parentGameObjectName' is not null or empty.
        /// </summary>
        /// <param name="component"></param>
        /// <param name="gameObjectName"></param>
        /// <param name="parentGameObjectName"></param>
        /// <returns></returns>
        public static Component InstantiateTransient(Type component, string gameObjectName, string parentGameObjectName = null)
        {
            if (component == null || string.IsNullOrEmpty(gameObjectName))
            {
#if LOG_WARNING
                Debug.LogWarningFormat("Can not instantiate transient object because arguments are not valid!");
#endif
                return null;
            }

            if (MFramework.TransientGameObject == null)
            {
#if LOG_WARNING
                Debug.LogWarningFormat("Can not instantiate transient object because 'MFramework.TransientGameObject' is null!");
#endif
                return null;
            }

            GameObject componentGo = new GameObject(gameObjectName);
            Component result = componentGo.AddComponent(component);
            Transform transientTransform = MFramework.TransientGameObject.transform;

            if (!string.IsNullOrEmpty(parentGameObjectName))
            {
                Transform currentParent = null;
                Transform existingParent = transientTransform.Find(parentGameObjectName);

                if (existingParent == null)
                    currentParent = new GameObject(parentGameObjectName).transform;
                else
                    currentParent = existingParent;

                currentParent.SetParent(transientTransform);
                componentGo.transform.SetParent(currentParent);
            }
            else
                componentGo.transform.SetParent(transientTransform);

            return result;
        }

        /// <summary>
        /// Create a game object then add TComponenet to it and make it Persistent sub child.
        /// </summary>
        /// <typeparam name="TComponent">Inherited from FComponent</typeparam>
        ///<param name="gameObjectName">Name of the game object that TComponent will add to it. By default it will set to TComponent full name.</param>
        ///<param name="subcategory">Name of parent game object. By default it will going to "Other" subcategory.</param>
        /// <returns>The instantiated component.</returns>
        public static TComponent InstantiatePersistent<TComponent>(string gameObjectName = null, string subcategory = null) where TComponent : FComponent
        {
            if (string.IsNullOrEmpty(gameObjectName)) gameObjectName = typeof(TComponent).ToString();

            GameObject persistentChildObject = new GameObject(gameObjectName, typeof(TComponent));

            if (string.IsNullOrEmpty(subcategory)) subcategory = PersistentSubCategories.OTHER;

            FPersistent.Persist(persistentChildObject, subcategory);

            return persistentChildObject.GetComponent<TComponent>();
        }

        /// <summary>
        /// Clone an object from original object and make it Persistent sub child.
        /// </summary>
        ///<param name="gameObjectName">Instantiated object's name. By default it set to TComponenet full name.</param>
        ///<param name="subcategory">The subcategory of persistent object. By default it will going to Other subcategory.</param>
        /// <returns>The instantiated GameObject.</returns>
        public static GameObject InstantiatePersistent(GameObject original, string subcategory = null)
        {
            if (original == null) return null;

            if (string.IsNullOrEmpty(subcategory)) subcategory = PersistentSubCategories.OTHER;

            GameObject persistentChildObject = Instantiate(original);

            FPersistent.Persist(persistentChildObject, subcategory);

            return persistentChildObject;
        }


        /// <summary>
        /// Clone a object from original object and make it Persistent sub child.
        /// </summary>
        /// <typeparam name="TComponent">Inherited from UnityEngine.Componenet</typeparam>
        ///<param name="gameObjectName">Instantiated object's name. By default it set to TComponenet full name.</param>
        ///<param name="subcategory">The subcategory of persistent object. By default it will going to Other subcategory.</param>
        /// <returns>The instantiated component.</returns>
        public static TComponenet InstantiatePersistent<TComponenet>(TComponenet original, string subcategory = null) where TComponenet : Component
        {
            if (original == null) return default(TComponenet);

            TComponenet persistentChildObject = Instantiate(original);

            FPersistent.Persist(persistentChildObject, subcategory);

            return persistentChildObject;
        }

        /// <summary>
        /// Add component to subcategory game object and make it persistent.
        /// </summary>
        ///<param name="subcategory">The subcategory of persistent object. By default it will going to Other subcategory.</param>
        /// <returns>The instantiated component.</returns>
        public static Component InstantiatePersistent(Type component, string subcategory = null, string parentName = null)
        {
            if (component == null) return null;

            if (string.IsNullOrEmpty(subcategory)) subcategory = PersistentSubCategories.OTHER;
            if (string.IsNullOrEmpty(parentName)) parentName = subcategory;

            var subcategoryGo = new GameObject(subcategory);

            Component result = subcategoryGo.AddComponent(component);

            FPersistent.Persist(subcategoryGo, parentName);

            return result;
        }

        #endregion
    }
}