﻿using System;

namespace Revy.Framework
{
    [CDisableAutoInstantiation]
    public class FDefaultGameManager : IGameManager, IDefaultGameManager
    {
        public Type InterfaceOfGameSystem => typeof(IDefaultGameManager);
    }
}