﻿
namespace Revy.Framework
{
    public interface ICanInvokeFixedTick : IFixedTick
    {
        bool CanInvokeFixedTick { get; set; }
    }
}