﻿using System.Collections;

namespace Revy.Framework
{
    public interface IInitializable : System.IComparable<IInitializable>
    {
        #region Properties

        /// <summary>
        /// Execution order of this class.
        /// By default it set to 100, smaller value will execute sooner.
        /// It must set to positive value. Framework use negative value for it's components.
        /// </summary>
        int ExecutionOrder { get; }
        bool IsInitializing { get; }
        bool HasInitialized { get; }
        bool HasShutdown { get; }

        #endregion

        #region Methods

        /// <summary>
        /// PreInitialization is called when the script instance is being created.
        /// PreInitialization is used to set order of script execution.
        /// For initialization use Initialize().
        /// </summary>
        /// <returns></returns>
        void PreInitialization();

        /// <summary>
        /// Initialize is called after all PreInitialization() is called.
        /// Initialize is used to initialize any variables or game state before the game starts.
        /// Initialize is called only once during the lifetime of the script instance.
        /// Use Initialize instead of the constructor for initialization.
        /// Initialize is called by coroutine.
        /// Always return true when initialization finished.
        /// </summary>
        /// <returns></returns>
        System.Threading.Tasks.Task Initialize();

        /// <summary>
        /// LoadResources is called when the initialization for all IObject instances finished.
        /// LoadResources is used to load any resources that this object needs.
        /// LoadResources is called only once during the lifetime of the script instance.
        /// Always return true when loading resources finished.
        /// Initialize is called by coroutine.
        /// </summary>
        /// <returns></returns>
        //System.Threading.Tasks.Task LoadResources();

        /// <summary>
        /// BeginPlay is called when the initialization and resource loading for all IObject is finished.
        /// BeginPlay is called only once during the lifetime of the script instance.
        /// Always return true at the end of method.
        /// BeginPlay is called by coroutine.
        /// </summary>
        /// <returns></returns>
        System.Threading.Tasks.Task BeginPlay();

        /// <summary>
        /// This function is called when the IObject will be destroyed.
        /// Always return true at the end of method.
        /// This function is called by coroutine.
        /// </summary>
        /// <returns></returns>
        System.Threading.Tasks.Task Shutdown();
        #endregion
    }
}