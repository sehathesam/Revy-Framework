﻿
namespace Revy.Framework
{
    public interface ICanInvokeTick : ITick
    {
        bool CanInvokeTick { get; set; }

        bool CanInvokeLateTick { get; set; }

        bool CanInvokeFixedTick { get; set; }
    }
}