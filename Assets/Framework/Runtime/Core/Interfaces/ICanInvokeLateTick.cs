﻿
namespace Revy.Framework
{
    public interface ICanInvokeLateTick : ILateTick
    {
        bool CanInvokeLateTick { get; set; }
    }
}