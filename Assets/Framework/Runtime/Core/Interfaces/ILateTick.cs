﻿namespace Revy.Framework
{
    public interface ILateTick
    {
        /// <summary>
        /// Tick is called every frame, if the FObject is enabled.
        /// </summary>
        void LateTick();
    }
}
