﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;
using Debug = UnityEngine.Debug;

#pragma warning disable 168
namespace Revy.Framework
{
    /// <summary>
    /// MFramework  is the base management class for entire framework system. 
    /// It is lone mono behavior object in entire framework system.
    /// All initialization and ticking behavior of FBase object is controlled here. 
    /// </summary>
    public sealed class MFramework : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Contains configuration of framework manager.
        /// </summary>
        public static FSFrameworkConfig Config { get; private set; }
#if LOG
        /// <summary>
        /// Contains framework begin initialization time stamp.
        /// </summary>
        private static float _startTimeStamp;
#endif
        /// <summary>
        /// All FComponent and FClass Objects will register their instances inside their parent's Awake() method.
        /// Registration process is automatic.
        /// </summary>
        private static List<IInitializable> _initializableObjects = new List<IInitializable>();

        /// <summary>
        /// Cache registered objects count to maintain performance.
        /// </summary>
        private static int _initializableObjectsCount;

        /// <summary>
        /// Contains objects that instantiates inside Initializing phase of framework.
        /// All FComponent and FClass Objects will register their instances inside their parent's Awake() method.
        /// Registration process is automatic.
        /// </summary>
        private static Queue<IInitializable> _nestedInitializableObjects = new Queue<IInitializable>();

        /// <summary>
        /// Contains objects that needs to receive Pause callbacks.
        /// </summary>
        private static List<IPauseable> _pauseableObjects = new List<IPauseable>();

        /// <summary>
        /// Contains objects that want to get Tick callback.
        /// Objects can register them self to this list by setting CanInvokeTick = true;
        /// Objects can unregister them self to this list by setting CanInvokeTick = false;
        /// </summary>
        private static List<ITick> _objectsToTick = new List<ITick>();

        /// <summary>
        /// Contains array type of '_registeredObjectsForTick' because array is faster to iterate over List.
        /// </summary>
        private static ITick[] _objectsToTickArray;

        private static int _objectsToTickCount;

        /// <summary>
        /// Contains objects that want to get LateTick callback.
        /// Objects can register them self to this list by setting CanInvokeLateTick = true;
        /// Objects can unregister them self to this list by setting CanInvokeLateTick = false;
        /// </summary>
        private static List<ILateTick> _objectsToLateTick = new List<ILateTick>();

        /// <summary>
        /// Contains array type of '_registeredObjectsForLateTick' because array is faster to iterate over List.
        /// </summary>
        private static ILateTick[] _objectsToLateTickArray;

        private static int _objectsToLateTickCount;

        /// <summary>
        /// Contains objects that want to get FixedTick callback.
        /// Objects can register them self to this list by setting CanInvokeFixedTick = true;
        /// Objects can unregister them self to this list by setting CanInvokeFixedTick = false;
        /// </summary>
        private static List<IFixedTick> _objectsToFixedTick = new List<IFixedTick>();

        /// <summary>
        /// Contains array type of '_objectsToFixedTick' because array is faster to iterate over List.
        /// </summary>
        private static IFixedTick[] _objectsToFixedTickArray;

        private static int _objectsToFixedTickCount;

        /// <summary>
        /// Contains list of all subsystems that instantiated.
        /// </summary>
        private static List<ISubsystem> _subsystems = new List<ISubsystem>();

        /// <summary>
        /// Contains list of all game systems that instantiated.
        /// </summary>
        private static List<IGameSystem> _gameSystems = new List<IGameSystem>();

#pragma warning disable 414
        private static readonly string LOG_TAG = "<color=olive><b>[Framework]</b></color>";
#pragma warning restore 414

        private static bool _isPlayBegun;

        #endregion

        #region Properties

        public const string ConfigAssetName = "FrameworkConfig";

        public const string FrameworkAssemblyName = "Revy.Framework";

        /// <summary>
        /// A reference to persistent game object in the scene.
        /// </summary>
        public static GameObject PersistentGameObject { get; private set; }

        /// <summary>
        /// A reference to transient game object in the scene. 
        /// </summary>
        public static GameObject TransientGameObject { get; private set; }

        public static bool IsPaused { get; private set; }

        /// <summary>
        /// Instance of created game manager.
        /// </summary>
        public static IGameManager GameManager { get; private set; }

        /// <summary>
        /// Is framework in initializing state.
        /// After initializing finished it's value is always true.
        /// </summary>
        public static bool IsInitializing { get; private set; }

        /// <summary>
        /// Is framework in initialized state.
        /// After initializing finished it's value is always true.
        /// </summary>
        public static bool HasInitialized { get; private set; }

        /// <summary>
        /// After initializing finished it's value is always true.
        /// </summary>
        public static bool IsPlayBegun => _isPlayBegun;

        #endregion

        #region Events

        /// <summary>
        /// This event will raise when all BeginPlay() callbacks of all FComponents and FClasses invoked.
        /// This event is called after All BeginPlay() and before first Tick().
        /// </summary>
        public static event Action OnPlayBegun;

        #endregion

        #region MonoBehaviour

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Startup()
        {
#if LOG
            _startTimeStamp = Time.realtimeSinceStartup;
#endif
            LoadFrameworkConfig();
            if (Config == null)
            {
                CLog.Error("Framework config not found.", category: LOG_TAG);
                return;
            }

            if (Config.DontStartFramework)
            {
                CLog.Log("Framework is not allowed to start.", category: LOG_TAG);
                return;
            }

            IsInitializing = false;
            HasInitialized = false;
            _isPlayBegun = false;

            MFramework[] existingFramework = FindObjectsOfType<MFramework>();
            foreach (var t in existingFramework) DestroyImmediate(t);

            PersistentGameObject = new GameObject("Persistent", new Type[] {typeof(MFramework), typeof(FPersistent)});
            PersistentGameObject.AddComponent<UnityEngine.EventSystems.EventSystem>();
            PersistentGameObject.AddComponent<UnityEngine.EventSystems.StandaloneInputModule>();

            TransientGameObject = new GameObject("Transient");

            SceneManager.sceneLoaded += (s, l) =>
            {
                if (TransientGameObject == null)
                {
                    TransientGameObject = new GameObject("Transient");
                }
            };
        }

        private void Start()
        {
            if (IsBuildTypeSpecified())
                StartFramework();
            else
                CLog.Warning($" Framework does not start because build setting does have problem.", category: LOG_TAG);
        }

        private void Update()
        {
            if (!_isPlayBegun) return;
            for (int i = 0; i < _objectsToTickCount; ++i)
            {
                ITick fObject = _objectsToTickArray[i];
                fObject?.Tick();
            }
        }

        private void LateUpdate()
        {
            if (!_isPlayBegun) return;
            for (int i = 0; i < _objectsToLateTickCount; ++i)
            {
                ILateTick fObject = _objectsToLateTickArray[i];
                fObject?.LateTick();
            }
        }

        private void FixedUpdate()
        {
            if (!_isPlayBegun) return;
            for (int i = 0; i < _objectsToFixedTickCount; ++i)
            {
                IFixedTick fObject = _objectsToFixedTickArray[i];
                fObject?.FixedTick();
            }
        }

        private async void OnDestroy()
        {
            if (_initializableObjects == null || _initializableObjects.Count <= 0) return;

            int count = _initializableObjects.Count;
            for (int i = 0; i < count; i++)
            {
                var obj = _initializableObjects[i];
                await obj.Shutdown();
                CUtilities.SetPropertyByReflection("HasShutdown", obj, true);
            }
        }

        #endregion

        #region Class Interface

        public static void Register(System.Object obj)
        {
            if (obj is ISubsystem subsystem)
            {
                if (!_subsystems.Contains(subsystem))
                    _subsystems.Add(subsystem);
            }

            if (obj is IGameSystem gameSystem)
            {
                if (!_gameSystems.Contains(gameSystem))
                    _gameSystems.Add(gameSystem);
            }

            if (obj is IInitializable)
            {
                _AddToInitializeList((IInitializable) obj);
            }

            if (obj is IPauseable)
            {
                _AddToPauseableList((IPauseable) obj);
            }

            if (!(obj is ICanInvokeTick) && obj is ITick)
            {
                AddtoTickList((ITick) obj);
            }

            if (!(obj is ICanInvokeLateTick) && obj is ILateTick)
            {
                AddToLateTickList((ILateTick) obj);
            }

            if (!(obj is ICanInvokeFixedTick) && obj is IFixedTick)
            {
                AddToFixedTickList((IFixedTick) obj);
            }
        }

        public static async void UnRegister(System.Object obj)
        {
            if (obj == null)
            {
                CLog.Warning($" Can not unregister null object.", category: LOG_TAG);
                return;
            }

            if (obj is IInitializable initializeAbleObj)
            {
                try
                {
                    if (initializeAbleObj.HasShutdown == false)
                    {
                        await initializeAbleObj.Shutdown();
                        CUtilities.SetPropertyByReflection("HasShutdown", obj, true);
                        _initializableObjects.Remove(initializeAbleObj);
                    }
                }
                catch (Exception e)
                {
                    CLog.Exception(e);
                }
            }

            UpdateInitializableObjectsCount();
            if (obj is ITick) RemoveFromTickList((ITick) obj);
            if (obj is ILateTick) RemoveFromLateTickList((ILateTick) obj);
            if (obj is IFixedTick) RemoveFromFixedTickList((IFixedTick) obj);
            if (obj is ISubsystem) _subsystems.Remove((ISubsystem) obj);
            if (obj is IGameSystem) _gameSystems.Remove((IGameSystem) obj);
        }

        public static void AddtoTickList(ITick obj)
        {
            if (obj == null)
            {
#if LOG_WARNING
                Debug.LogWarningFormat($"{LOG_TAG} Can not register null object.");
#endif
                return;
            }

            if (!_objectsToTick.Contains(obj))
                _objectsToTick.Add(obj);

            UpdateRegisteredObjectsForTickCount();
            _objectsToTickArray = _objectsToTick.ToArray();
        }

        public static void RemoveFromTickList(ITick obj)
        {
            if (obj == null)
            {
#if LOG_WARNING
                Debug.LogWarningFormat($"{LOG_TAG} Can not unregister null object.");
#endif
                return;
            }

            _objectsToTick.Remove(obj);

            UpdateRegisteredObjectsForTickCount();
            _objectsToTickArray = _objectsToTick.ToArray();
        }

        public static void AddToLateTickList(ILateTick obj)
        {
            if (obj == null)
            {
#if LOG_WARNING
                Debug.LogWarningFormat($"{LOG_TAG} Can not register null object.");
#endif
                return;
            }

            if (!_objectsToLateTick.Contains(obj))
                _objectsToLateTick.Add(obj);

            UpdateRegisteredObjectsForLateTickCount();
            _objectsToLateTickArray = _objectsToLateTick.ToArray();
        }

        public static void RemoveFromLateTickList(ILateTick obj)
        {
            if (obj == null)
            {
#if LOG_WARNING
                Debug.LogWarningFormat($"{LOG_TAG} Can not unregister null object.");
#endif
                return;
            }

            _objectsToLateTick.Remove(obj);
            UpdateRegisteredObjectsForLateTickCount();
            _objectsToLateTickArray = _objectsToLateTick.ToArray();
        }

        public static void AddToFixedTickList(IFixedTick obj)
        {
            if (obj == null)
            {
#if LOG_WARNING
                Debug.LogWarningFormat($"{LOG_TAG} Can not register null object.");
#endif
                return;
            }

            if (!_objectsToFixedTick.Contains(obj))
                _objectsToFixedTick.Add(obj);

            UpdateRegisteredObjectsForFixedTickCount();
            _objectsToFixedTickArray = _objectsToFixedTick.ToArray();
        }

        public static void RemoveFromFixedTickList(IFixedTick obj)
        {
            if (obj == null)
            {
#if LOG_WARNING
                Debug.LogWarningFormat($"{LOG_TAG} Can not unregister null object.");
#endif
                return;
            }

            _objectsToFixedTick.Remove(obj);
            UpdateRegisteredObjectsForFixedTickCount();
            _objectsToFixedTickArray = _objectsToFixedTick.ToArray();
        }

        public static async void StartFramework()
        {
            if (!IsInitializing && !HasInitialized)
            {
                float oldTime = Time.realtimeSinceStartup;

                GarbageCollector.GCMode = GarbageCollector.Mode.Disabled;
                await SetupSubsystems();
                await SetupGamesystems();
                GarbageCollector.GCMode = GarbageCollector.Mode.Enabled;

                CLog.Log($"Total loading takes {Time.realtimeSinceStartup - oldTime} seconds.", category: LOG_TAG);
            }
        }

        public static void PrintRegistredObjects()
        {
            Debug.Log("-----------------------------------------------");
            Debug.Log($"Registered Objects Count: {_initializableObjects.Count}");
            foreach (var item in _initializableObjects)
            {
                Debug.Log(item.GetType().Name);
            }

            Debug.Log("-----------------------------------------------");
            Debug.Log($"Tick Objects Count: {_objectsToTick.Count}");
            foreach (var item in _objectsToTick)
            {
                Debug.Log(item.GetType().Name);
            }

            Debug.Log("-----------------------------------------------");
            Debug.Log($"FixedTick Objects Count: {_objectsToFixedTick.Count}");
            foreach (var item in _objectsToFixedTick)
            {
                Debug.Log(item.GetType().Name);
            }

            Debug.Log("-----------------------------------------------");
            Debug.Log($"LateTick Objects Count: {_objectsToLateTick.Count}");
            foreach (var item in _objectsToLateTick)
            {
                Debug.Log(item.GetType().Name);
            }
        }

        private static async Task SetupSubsystems()
        {
            var oldTime = Time.realtimeSinceStartup;
            InstantiateSubsystems();
            RegisterSubsystemsIntoDISystem(); //Automatic DI registration.
            await InitializeSubsystems();
            CLog.Log($"Loading Subsystems takes {Time.realtimeSinceStartup - oldTime} seconds.");
        }

        private static void InstantiateSubsystems()
        {
            Type[] allSubsystems = CUtilities.GetAllImplementingTypes(typeof(ISubsystem));

            if (allSubsystems == null)
            {
                CLog.Log(" Can not find any subsystems in this assembly when trying to instantiate subsystems.",
                    category: LOG_TAG);
                return;
            }

            int count = allSubsystems.Length;
            for (int i = 0; i < count; i++)
            {
                Type subsystem = allSubsystems[i];

                if (subsystem.IsDefined(typeof(CDisableAutoInstantiationAttribute), false)) continue;
                if (subsystem.IsAbstract) continue;
                if (!Config.IsSubsystemEnable(subsystem)) continue;

                if (subsystem.IsSubclassOf(typeof(FComponent)))
                {
                    FComponent.InstantiatePersistent(subsystem, subsystem.Name, PersistentSubCategories.SUBSYSTEMS);
                }
                else
                {
                    Activator.CreateInstance(subsystem);
                }
            }
        }

        private static void RegisterSubsystemsIntoDISystem()
        {
            int subsystemsCount = _subsystems.Count;
            for (int i = 0; i < subsystemsCount; i++)
            {
                ISubsystem subsystem = _subsystems[i];
                if (subsystem != null)
                {
                    if (subsystem.InterfaceOfSubsystem != null)
                    {
                        CServiceLocator.Register(subsystem.InterfaceOfSubsystem, subsystem);
                    }
                    else
                    {
                        CLog.Error($"'FSubsystem.InterfaceOfSubsystem' is null on '{subsystem.GetType().Name}' object.",
                            category: LOG_TAG);
                    }
                }
            }
        }

        private static void RegisterGameSystemsIntoDISystem()
        {
            int gameSystemsCount = _gameSystems.Count;
            for (int i = 0; i < gameSystemsCount; i++)
            {
                IGameSystem gameSystem = _gameSystems[i];
                if (gameSystem != null)
                {
                    if (gameSystem.InterfaceOfGameSystem != null)
                    {
                        CServiceLocator.Register(gameSystem.InterfaceOfGameSystem, gameSystem);
                    }
                    else
                    {
                        CLog.Error(
                            $"'FGameSystem.InterfaceOfGameSystem' is null on '{gameSystem.GetType().Name}' object.",
                            category: LOG_TAG);
                    }
                }
            }
        }

        private static async Task SetupGamesystems()
        {
            var oldTime = Time.realtimeSinceStartup;

            SetupGameManager();
            InstantiateGameSystems();
            RegisterGameSystemsIntoDISystem();
            await InitializeGameSystems();

            CLog.Log($"Loading Game Systems takes {Time.realtimeSinceStartup - oldTime} seconds.", category: LOG_TAG);
        }

        private static void SetupGameManager()
        {
            var frameworkConfig = MFramework.Config;
            if (frameworkConfig == null) return;

            // Cache GameManagerType to maintain performance. GameManagerType takes a lot of memory and CPU time.
            var gameManagerType = frameworkConfig.GameManagerType;
            if (gameManagerType == null)
            {
                CLog.Log(
                    "Can not create <b>Game Manager</b> because type of Game Manager does not specified in frameworks configurations.",
                    category: LOG_TAG);
                return;
            }

            if (typeof(FComponent).IsAssignableFrom(gameManagerType))
            {
                GameManager = FComponent.InstantiatePersistent(gameManagerType,
                    subcategory: gameManagerType.Name, parentName: "GameSystems") as IGameManager;
            }
            else
            {
                GameManager = Activator.CreateInstance(gameManagerType) as IGameManager;
            }

            if (GameManager == null)
            {
                CLog.Error($"Can not create game manger component.", category: LOG_TAG);
                return;
            }

            CLog.Log(
                $"Game Manager Class(<b>{gameManagerType}</b>) successfully instantiated.", category: LOG_TAG);
        }

        private static void InstantiateGameSystems()
        {
            Type[] gameSystems = CUtilities.GetAllImplementingTypes(typeof(IGameSystem));
            if (gameSystems == null || gameSystems.Length == 0) return;

            int gameSystemsCount = gameSystems.Length;
            for (int i = 0; i < gameSystemsCount; i++)
            {
                Type gameSystem = gameSystems[i];
                if (!IsValidGameSystem(gameSystem)) continue;
                if (gameSystem.IsSubclassOf(typeof(FComponent)))
                {
                    FComponent.InstantiatePersistent(gameSystem, subcategory: gameSystem.Name,
                        parentName: "GameSystems");
                }
                else
                {
                    Activator.CreateInstance(gameSystem);
                }
            }
        }

        private static bool IsValidGameSystem(Type gameSystem)
        {
            if (gameSystem == null) return false;

            if (gameSystem.IsInterface || gameSystem.IsAbstract) return false;

            if (IsGameManager(gameSystem)) return false;

            if (HasDisableInstantiationAttribute(gameSystem)) return false;

            if (!HasCorrectGameManagerAttribute(gameSystem)) return false;

            if (!Config.IsGameSystemEnable(gameSystem)) return false;

            return true;
        }

        private static bool HasDisableInstantiationAttribute(Type gameSystem)
        {
            return gameSystem.IsDefined(typeof(CDisableAutoInstantiationAttribute));
        }

        private static bool IsGameManager(Type gameSystem)
        {
            return typeof(IGameManager).IsAssignableFrom(gameSystem);
        }

        private static bool HasCorrectGameManagerAttribute(Type gameSystem)
        {
            CSetGameManagerAttribute gameManagerAttribute =
                gameSystem.GetCustomAttribute(typeof(CSetGameManagerAttribute)) as CSetGameManagerAttribute;

            if (gameManagerAttribute == null)
            {
#if LOG_WARNING
                Debug.LogWarning(
                    $"{LOG_TAG} '{gameSystem.Name}' does not have 'CSetGameManager' attribute. All game systems must have this attribute.");
#endif
                return false;
            }

            Type gameManager = gameManagerAttribute.GetGameManagerType();

            if (gameManager == null)
            {
#if LOG_ERROR
                Debug.LogError($"{LOG_TAG} Constructor argument of CSetGameMnager in '{gameSystem.Name}' is null. ");
#endif
                return false;
            }

            if (Config == null || Config.GameManagerType == null) return false;

            if (gameManager != Config.GameManagerType)
            {
                //Debug.Log($"<b>{gameSystem.Name}</b> game system has <b>{gameManager.Name}</b> game manager type and does not instantiated. Current game manager is <b>{Config.GameManagerType.Name}</b>.");
                return false;
            }

            return true;
        }

        private static async Task InitializeSubsystems()
        {
            IsInitializing = true;
            HasInitialized = false;

            await InitializeFComponents(isOnlySubsystems: true, isInitializing: false, hasInitialized: true);

            //Reset initializing state for game systems.
            IsInitializing = false;
            HasInitialized = false;
        }

        public static async void ShutdownFramework()
        {
            if (!_isPlayBegun) return;

            await ShutdownRegistredObjects();
        }

        public static void SetPause(bool value = true)
        {
            if (value)
                PauseFramework();
            else
                ResumeFramework();

            IsPaused = value;
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Register an object to the framework to run framework events.
        /// </summary>
        /// <param name="obj"></param>
        private static async void _AddToInitializeList(IInitializable obj)
        {
            if (obj == null)
            {
                CLog.Warning($"{LOG_TAG} Can not register null object.");
                return;
            }

            if (IsInitializing)
            {
                if (!_nestedInitializableObjects.Contains(obj))
                {
                    _nestedInitializableObjects.Enqueue(obj);
                }
            }
            else if (HasInitialized == false)
            {
                if (!_initializableObjects.Contains(obj))
                {
                    _initializableObjects.Add(obj);
                    UpdateInitializableObjectsCount(); // cache registered objects count.
                }
            }
            else if (IsInitializing == false && HasInitialized == true)
            {
                await InitializeSingleObject(obj);
            }
        }

        private static void _AddToPauseableList(IPauseable pauseableObject)
        {
            if (!_pauseableObjects.Contains(pauseableObject))
            {
                _pauseableObjects.Add(pauseableObject);
            }
        }

        /// <summary>
        /// Load configuration from Resources folder.
        /// This method should load framework config synchronously.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void LoadFrameworkConfig()
        {
            Config = Resources.Load<FSFrameworkConfig>(ConfigAssetName);
        }

        /// <summary>
        /// Invokes PreInitialization(),Initialize(),LoadResources(),BeginPlay() on registered IFObjects.
        /// First subsystem will initialized then other objects.
        /// Will invoke in Start().
        /// </summary>
        /// <returns></returns>
        private static async Task InitializeGameSystems()
        {
            if (GameManager == null)
            {
                IsInitializing = false;
                HasInitialized = false;
                _isPlayBegun = false;
                return;
            }

            IsInitializing = true;
            HasInitialized = false;

            await InitializeFComponents(isOnlySubsystems: false, isInitializing: false, hasInitialized: true);

            _isPlayBegun = true;

            OnPlayBegun?.Invoke();
        }

        /// <summary>
        /// Wrap two functionality of register subsystem and other register objects into one method.
        /// </summary>
        /// <param name="isOnlySubsystems"></param>
        /// <param name="isInitializing"></param>
        /// <param name="hasInitialized"></param>
        /// <returns></returns>
        private static async Task InitializeFComponents(bool isOnlySubsystems, bool isInitializing, bool hasInitialized)
        {
            //If we are going to initialize game systems and FComponents, inject subsystems first.
            if (!isOnlySubsystems)
            {
                InjectDependenciesOfType(typeof(ISubsystem));
            }

            InvokePreInitialize(isOnlySubsystems);

            await InvokeInitialize(isOnlySubsystems);

            await InitializeNestedObjects();

            SetInitializationState(isInitializing, hasInitialized);

            if (isOnlySubsystems)
                InjectDependenciesOfType(typeof(ISubsystem));
            else
                InjectDependenciesOfType(null);

            IndexObjectsForTerminal(isOnlySubsystems);

            await InvokeBeginPlay(isOnlySubsystems);
        }

        /// <summary>
        /// Invoke preinitialize on registered objects.
        /// </summary>
        /// <param name="isOnlySubsystems"></param>
        /// <returns></returns>
        private static void InvokePreInitialize(bool isOnlySubsystems)
        {
            //Invokes PreInitialize()
            for (int i = 0; i < _initializableObjectsCount; ++i)
            {
                IInitializable registeredItem = _initializableObjects[i];
                if (registeredItem == null || registeredItem.HasInitialized) continue;
                if (isOnlySubsystems && (registeredItem is ISubsystem) == false)
                    continue; //Do not proceed if item is not Subsystem.
                if (!isOnlySubsystems && (registeredItem is ISubsystem))
                    continue; //Do not proceed if we want just invoke game systems.

                CUtilities.SetPropertyByReflection("HasShutdown", registeredItem,
                    false); //Set IFBase objects initializing state.

                registeredItem.PreInitialization();
            }
        }

        /// <summary>
        /// Invoke initialize on registered objects.
        /// </summary>
        /// <param name="isOnlySubsystems"></param>
        /// <returns></returns>
        private static async Task InvokeInitialize(bool isOnlySubsystems)
        {
            //Invokes Initialize()
            for (int i = 0; i < _initializableObjectsCount; ++i)
            {
                IInitializable registeredItem = _initializableObjects[i];
                if (registeredItem == null || registeredItem.HasInitialized) continue;
                if (isOnlySubsystems && (registeredItem is ISubsystem) == false)
                    continue; //Do not proceed if item is not Subsystem.
                if (!isOnlySubsystems && (registeredItem is ISubsystem))
                    continue; //Do not proceed if we want just invoke game systems.

                CUtilities.SetPropertyByReflection("IsInitializing", registeredItem,
                    true); // Set IFBase objects initializing state.

                await registeredItem.Initialize();
                CUtilities.SetPropertyByReflection("HasInitialized", registeredItem,
                    true); // Set IFBase objects initializing state.
                CUtilities.SetPropertyByReflection("IsInitializing", registeredItem, false);
            }
        }

        private static void SetInitializationState(bool isInitializingState, bool hasInitialized)
        {
            IsInitializing = isInitializingState;
            HasInitialized = hasInitialized;
        }

        private static void InjectDependenciesOfType(Type type)
        {
            for (int i = 0; i < _initializableObjectsCount; ++i)
            {
                var registeredItem = _initializableObjects[i];

                if (registeredItem == null) continue;

                CDependencyInjection.Inject(registeredItem, type);
            }
        }

        private static void IndexObjectsForTerminal(bool isOnlySubsystems)
        {
            for (int i = 0; i < _initializableObjectsCount; ++i)
            {
                var registeredItem = _initializableObjects[i];

                if (registeredItem == null) continue;

                if (isOnlySubsystems && (registeredItem is ISubsystem) == false) continue;

                FTerminal.IndexObject(registeredItem);
            }
        }

        /// <summary>
        /// Invoke BeginPlay on registered objects.
        /// </summary>
        /// <param name="isOnlySubsystems"></param>
        /// <returns></returns>
        private static async Task InvokeBeginPlay(bool isOnlySubsystems)
        {
            //Invokes BeginPlay()
            for (int i = 0; i < _initializableObjectsCount; ++i)
            {
                var registeredItem = _initializableObjects[i];
                if (registeredItem == null) continue;
                if (isOnlySubsystems && (registeredItem is ISubsystem) == false)
                    continue; //Do not proceed if item is not Subsystem.
                if (!isOnlySubsystems && (registeredItem is ISubsystem))
                    continue; //Do not proceed if we want just invoke game systems.
                await registeredItem.BeginPlay();
            }
        }

        /// <summary>
        /// Initialize nested objects, but do not call their BeginPlay() callback.
        /// their BeginPlay() will called with other registered objects.
        /// </summary>
        private static async Task InitializeNestedObjects()
        {
            while (_nestedInitializableObjects.Count != 0)
            {
                IInitializable nestedObject = _nestedInitializableObjects.Dequeue();

                CUtilities.SetPropertyByReflection("HasShutdown", nestedObject, false);

                if (!(nestedObject is ISubsystem)) CDependencyInjection.Inject(nestedObject, typeof(ISubsystem));

                nestedObject.PreInitialization();

                CUtilities.SetPropertyByReflection("IsInitializing", nestedObject, true);

                await nestedObject.Initialize();

                CUtilities.SetPropertyByReflection("HasInitialized", nestedObject,
                    true); // Set IFBase objects initializing state.
                CUtilities.SetPropertyByReflection("IsInitializing", nestedObject, false);

                if (!_initializableObjects.Contains(nestedObject))
                {
                    _initializableObjects.Add(nestedObject);
                }
            }

            UpdateInitializableObjectsCount();
        }

        private static async Task ShutdownRegistredObjects()
        {
            for (int i = 0; i < _initializableObjects.Count; ++i)
            {
                await _initializableObjects[i].Shutdown();
            }

            IsInitializing = false;
            HasInitialized = false;
            _isPlayBegun = false;
        }

        /// <summary>
        /// You should update _registeredObjectsCount field every time _registredObjects's count update.
        /// </summary>
        private static void UpdateInitializableObjectsCount()
        {
            if (_initializableObjects != null)
                _initializableObjectsCount = _initializableObjects.Count;
        }

        private static void UpdateRegisteredObjectsForTickCount()
        {
            if (_objectsToTick != null)
                _objectsToTickCount = _objectsToTick.Count;
        }

        private static void UpdateRegisteredObjectsForLateTickCount()
        {
            if (_objectsToLateTick != null)
                _objectsToLateTickCount = _objectsToLateTick.Count;
        }

        private static void UpdateRegisteredObjectsForFixedTickCount()
        {
            if (_objectsToFixedTick != null)
                _objectsToFixedTickCount = _objectsToFixedTick.Count;
        }

        private static async Task InitializeSingleObject(IInitializable obj)
        {
            CDependencyInjection.Inject(obj);
            obj.PreInitialization();
            CUtilities.SetPropertyByReflection("IsInitializing", obj, true);
            await obj.Initialize();

            FTerminal.IndexObject(obj);

            if (obj.HasInitialized == false)
            {
                CUtilities.SetPropertyByReflection("IsInitializing", obj, false);
                CUtilities.SetPropertyByReflection("HasInitialized", obj, true);
                await obj.BeginPlay();
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static bool IsBuildTypeSpecified()
        {
            bool isBuildSpecifies = false;
            bool isSpecifiedCorrectly = true;
            bool isBuildSettingsMatch = true;

#if DEVELOPMENT || DEVELOPMENT_RELEASE || PUBLIC_RELEASE
            isBuildSpecifies = true;
#endif

#if (DEVELOPMENT || DEVELOPMENT_RELEASE) && PUBLIC_RELEASE
            isSpecifiedCorrectly = false;
            Debug.LogError($"{LOG_TAG} Both <b>development build</b> and <b>release build</b> are specified as preprocessor directives. Make sure just one type of build is specified in preprocessor directives.");
#endif

#if DEVELOPMENT && !DEVELOPMENT_BUILD && !UNITY_EDITOR //DEVELOPMENT_BUILD is not exist in Editor. To prevent incorrect behavior, UNITY_EDITOR added.
            isBuildSettingsMatch = false;
            Debug.LogError($"{LOG_TAG} <b>Build type</b> in <b>Framework configuration</b> is specified as <b>Development</b> but unity build setting is not. Make sure these settings match.");
#endif

#if (DEVELOPMENT_RELEASE || PUBLIC_RELEASE) && DEVELOPMENT_BUILD
            isBuildSettingsMatch = false;
            Debug.LogError($"{LOG_TAG} <b>Build type</b> in <b>Framework configuration</b> is specified as <b>release</b> but unity build setting is development. Make sure these settings match.");
#endif
            if (!isBuildSpecifies)
                Debug.LogError($"Build type does not specified in Framework configuration.");

            return isBuildSpecifies && isSpecifiedCorrectly && isBuildSettingsMatch;
        }

        private static void PauseFramework()
        {
            if (!_isPlayBegun) return;

            for (int i = 0; i < _initializableObjectsCount; ++i)
            {
                _pauseableObjects[i].OnPause();
            }
        }

        private static void ResumeFramework()
        {
            if (!_isPlayBegun && !IsPaused) return;

            for (int i = 0; i < _initializableObjectsCount; ++i)
            {
                _pauseableObjects[i].OnResume();
            }
        }

        #endregion Helpers
    }
}