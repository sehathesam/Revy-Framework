﻿/**
 * Added by mahdi fada
 * Add Time: 8 / 16 / 2017
 * Description: Message
 **/


/*
 * Advanced C# messenger by Ilya Suzdalnitski. V1.0
 * 
 * Based on Rod Hyde's "CSharpMessenger" and Magnus Wolffelt's "CSharpMessenger Extended".
 * 
 * Features:
 	* Prevents a MissingReferenceException because of a reference to a destroyed message handler.
 	* Option to log all messages
 	* Extensive error detection, preventing silent bugs
 * 
 * Usage examples:
 	1. Messenger.AddListener<GameObject>("prop collected", PropCollected);
 	   Messenger.Broadcast<GameObject>("prop collected", prop);
 	2. Messenger.AddListener<float>("speed changed", SpeedChanged);
 	   Messenger.Broadcast<float>("speed changed", 0.5f);
 * 
 * Messenger cleans up its evenTable automatically upon loading of a new level.
 * 
 * Don't forget that the messages that should survive the cleanup, should be marked with Messenger.MarkAsPermanent(string)
 * 
 */

//#define LOG_ALL_MESSAGES
//#define LOG_ADD_LISTENER
//#define LOG_BROADCAST_MESSAGE
//#define REQUIRE_LISTENER

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Revy.Framework
{
    public delegate Task Callback();

    public delegate Task Callback<T>(T arg1);

    public delegate Task Callback<T, U>(T arg1, U arg2);

    public delegate Task Callback<T, U, V>(T arg1, U arg2, V arg3);

    public class CMessenger<EventType>
    {
        #region Internal variables

        private const int BIG_NUMBER = int.MaxValue;
        //Disable the unused variable warning
#pragma warning disable 0414
        //Ensures that the MessengerHelper will be created automatically upon start of the game.
        //private MessengerHelper messengerHelper = (new GameObject("MessengerHelper")).AddComponent<MessengerHelper>();
#pragma warning restore 0414

        public Dictionary<EventType, Delegate> eventTable = new Dictionary<EventType, Delegate>();

        //Message handlers that should never be removed, regardless of calling Cleanup
        public List<EventType> permanentMessages = new List<EventType>();

        #endregion

        #region Helper methods

        //Marks a certain message as permanent.
        public void MarkAsPermanent(EventType eventType)
        {
#if LOG_ALL_MESSAGES
            Debug.Log("Messenger MarkAsPermanent \t\"" + eventType + "\"");
#endif
            permanentMessages.Add(eventType);
        }


        public void Cleanup()
        {
#if LOG_ALL_MESSAGES
            Debug.Log("MESSENGER Cleanup. Make sure that none of necessary listeners are removed.");
#endif

            List<EventType> messagesToRemove = new List<EventType>();

            foreach (KeyValuePair<EventType, Delegate> pair in eventTable)
            {
                bool wasFound = false;

                foreach (EventType message in permanentMessages)
                {
                    //if (pair.Key == message)
                    if (pair.Key.Equals(message))
                    {
                        wasFound = true;
                        break;
                    }
                }

                if (!wasFound)
                    messagesToRemove.Add(pair.Key);
            }

            foreach (EventType message in messagesToRemove)
            {
                eventTable.Remove(message);
            }
        }

        public void PrintEventTable()
        {
            Debug.Log("\t\t\t=== MESSENGER PrintEventTable ===");

            foreach (KeyValuePair<EventType, Delegate> pair in eventTable)
            {
                Debug.Log("\t\t\t" + pair.Key + "\t\t" + pair.Value);
            }

            Debug.Log("\n");
        }

        #endregion

        #region Message logging and exception throwing

        private void OnListenerAdding(EventType eventType, Delegate listenerBeingAdded)
        {
            if (!eventTable.ContainsKey(eventType)) return;

            Delegate d = eventTable[eventType];
            if (d != null && d.GetType() != listenerBeingAdded.GetType())
            {
                throw new ListenerException(
                    $"Attempting to add listener with inconsistent signature for event instance {eventType}. Current listeners have instance {d.GetType().Name} and listener being added has instance {listenerBeingAdded.GetType().Name}");
            }
        }

        private void OnListenerRemoving(EventType eventType, Delegate listenerBeingRemoved)
        {
            if (eventTable.ContainsKey(eventType))
            {
                Delegate d = eventTable[eventType];

                if (d == null)
                {
                    throw new ListenerException(string.Format(
                        "Attempting to remove listener with for event instance \"{0}\" but current listener is null.",
                        eventType));
                }
                else if (d.GetType() != listenerBeingRemoved.GetType())
                {
                    throw new ListenerException(string.Format(
                        "Attempting to remove listener with inconsistent signature for event instance {0}. Current listeners have instance {1} and listener being removed has instance {2}",
                        eventType, d.GetType().Name, listenerBeingRemoved.GetType().Name));
                }
            }
            else
            {
                throw new ListenerException(string.Format(
                    "Attempting to remove listener for instance \"{0}\" but Messenger doesn't know about this event instance.",
                    eventType));
            }
        }

        private void OnListenerRemoved(EventType eventType)
        {
            if (eventTable[eventType] == null)
            {
                eventTable.Remove(eventType);
            }
        }

        private void OnBroadcasting(EventType eventType)
        {
#if REQUIRE_LISTENER
            if (!eventTable.ContainsKey(eventType))
            {
                throw new BroadcastException(string.Format("Broadcasting message \"{0}\" but no listener found. Try marking the message with Messenger.MarkAsPermanent.", eventType));
            }
#endif
        }

        private BroadcastException CreateBroadcastSignatureException(EventType eventType)
        {
            return new BroadcastException(string.Format(
                "Broadcasting message \"{0}\" but listeners have a different signature than the broadcaster.",
                eventType));
        }

        private class BroadcastException : Exception
        {
            public BroadcastException(string msg)
                : base(msg)
            {
            }
        }

        private class ListenerException : Exception
        {
            public ListenerException(string msg)
                : base(msg)
            {
            }
        }

        #endregion

        #region AddListener

        //No parameters
        public void AddListener(EventType eventType, Callback handler)
        {
            OnListenerAdding(eventType, handler);

            if (!eventTable.ContainsKey(eventType))
            {
                eventTable.Add(eventType, handler);
            }
            else if (!eventTable[eventType].GetInvocationList().Contains(handler))
            {
                eventTable[eventType] = (Callback)eventTable[eventType] + handler;
            }
        }

        //Single parameter
        public void AddListener<T>(EventType eventType, Callback<T> handler)
        {
            OnListenerAdding(eventType, handler);

            if (!eventTable.ContainsKey(eventType))
            {
                eventTable.Add(eventType, handler);
            }
            else if (!eventTable[eventType].GetInvocationList().Contains(handler))
            {
                eventTable[eventType] = (Callback<T>)eventTable[eventType] + handler;
            }
        }

        //Two parameters
        public void AddListener<T, U>(EventType eventType, Callback<T, U> handler)
        {
            OnListenerAdding(eventType, handler);

            if (!eventTable.ContainsKey(eventType))
            {
                eventTable.Add(eventType, handler);
            }
            else if (!eventTable[eventType].GetInvocationList().Contains(handler))
            {
                eventTable[eventType] = (Callback<T, U>)eventTable[eventType] + handler;
            }
        }

        //Three parameters
        public void AddListener<T, U, V>(EventType eventType, Callback<T, U, V> handler)
        {
            OnListenerAdding(eventType, handler);

            if (!eventTable.ContainsKey(eventType))
            {
                eventTable.Add(eventType, handler);
            }
            else if (!eventTable[eventType].GetInvocationList().Contains(handler))
            {
                eventTable[eventType] = (Callback<T, U, V>)eventTable[eventType] + handler;
            }
        }

        #endregion

        #region RemoveListener

        //No parameters
        public void RemoveListener(EventType eventType, Callback handler)
        {
            OnListenerRemoving(eventType, handler);
            eventTable[eventType] = (Callback)eventTable[eventType] - handler;
            OnListenerRemoved(eventType);
        }

        //Single parameter
        public void RemoveListener<T>(EventType eventType, Callback<T> handler)
        {
            OnListenerRemoving(eventType, handler);
            eventTable[eventType] = (Callback<T>)eventTable[eventType] - handler;
            OnListenerRemoved(eventType);
        }

        //Two parameters
        public void RemoveListener<T, U>(EventType eventType, Callback<T, U> handler)
        {
            OnListenerRemoving(eventType, handler);
            eventTable[eventType] = (Callback<T, U>)eventTable[eventType] - handler;
            OnListenerRemoved(eventType);
        }

        //Three parameters
        public void RemoveListener<T, U, V>(EventType eventType, Callback<T, U, V> handler)
        {
            OnListenerRemoving(eventType, handler);
            eventTable[eventType] = (Callback<T, U, V>)eventTable[eventType] - handler;
            OnListenerRemoved(eventType);
        }

        public void RemoveAllListener(object instance)
        {
            var eventTableTmp = new Dictionary<EventType, Delegate>(eventTable);
            foreach (Delegate listeners in eventTable.Values)
            {
                foreach (Delegate aMethod in listeners.GetInvocationList())
                {
                    if (aMethod.Target != null && aMethod.Target == instance)
                    {
                        Delegate listenerTmp = eventTableTmp.Values.First(value => value == listeners);
                        listenerTmp = Delegate.Remove(listenerTmp, aMethod);
                        if (listenerTmp == null)
                        {
                            var keyValueItem = eventTableTmp.First(item => item.Value == listeners);
                            eventTableTmp.Remove(keyValueItem.Key);
                        }
                    }
                }
            }
            eventTable = eventTableTmp;
        }
        #endregion

        #region Broadcast

        //No parameters listene
        public async void Broadcast(EventType eventType, EEventFilter filter = EEventFilter.None)
        {
#if LOG_ALL_MESSAGES || LOG_BROADCAST_MESSAGE
            //	Debug.Log("MESSENGER\t" + System.DateTime.Now.ToShortTimeString() + "\t\t\tInvoking \t\"" + eventType + "\"");
#endif
            OnBroadcasting(eventType);
            Delegate d;
            if (!eventTable.TryGetValue(eventType, out d)) return;

            var callback = d as Callback;

            if (callback == null)
            {
                throw CreateBroadcastSignatureException(eventType);
            }

            var sortDictionary = new Dictionary<int, int>();
            var index = 0;
            var invocationList = callback.GetInvocationList();
            foreach (var item in invocationList)
            {
                var it = item as Callback;

                if (!_validate(it) || _isFilitered(it, filter))
                {
                    index++;
                    continue;
                }

                var executionAttribute =
                    it.Method.GetCustomAttribute(typeof(CExecutionOrderAttribute)) as CExecutionOrderAttribute;
                sortDictionary.Add(index, executionAttribute?.Order ?? BIG_NUMBER);
                index++;
            }

            var sortedByOrder = sortDictionary.ToList();
            sortedByOrder.Sort((firstPair, nextPair) => firstPair.Value.CompareTo(nextPair.Value));
            foreach (var order in sortedByOrder)
            {
                var value = invocationList[order.Key] as Callback;
                if (value == null) continue;
                _logBroadCast(value, eventType);
                await value();
            }
        }

        //Single parameter
        public async void Broadcast<T>(EventType eventType, T arg1, EEventFilter filter = EEventFilter.None)
        {
#if LOG_ALL_MESSAGES || LOG_BROADCAST_MESSAGE
            //	Debug.Log("MESSENGER\t" + System.DateTime.Now.ToString("hh:mm:ss.fff") + "\t\t\tInvoking \t\"" + eventType + "\"");
#endif
            OnBroadcasting(eventType);
            Delegate d;
            if (!eventTable.TryGetValue(eventType, out d)) return;

            var callback = d as Callback<T>;

            if (callback == null)
            {
                throw CreateBroadcastSignatureException(eventType);
            }

            var sortDictionary = new Dictionary<int, int>();
            var index = 0;
            var invocationList = callback.GetInvocationList();
            foreach (var item in invocationList)
            {
                var it = item as Callback<T>;

                if (!_validate(it) || _isFilitered(it, filter))
                {
                    index++;
                    continue;
                }

                var executionAttribute =
                    it.Method.GetCustomAttribute(typeof(CExecutionOrderAttribute)) as CExecutionOrderAttribute;
                sortDictionary.Add(index, executionAttribute?.Order ?? BIG_NUMBER);
                index++;
            }

            var sortedByOrder = sortDictionary.ToList();
            sortedByOrder.Sort((firstPair, nextPair) => firstPair.Value.CompareTo(nextPair.Value));
            foreach (var order in sortedByOrder)
            {
                var value = invocationList[order.Key] as Callback<T>;
                if (value == null) continue;
                _logBroadCast(value, eventType);
                await value(arg1);
            }
        }

        //Two parameters
        public async void Broadcast<T, U>(EventType eventType, T arg1, U arg2, EEventFilter filter = EEventFilter.None)
        {
#if LOG_ALL_MESSAGES || LOG_BROADCAST_MESSAGE
//		Debug.Log("MESSENGER\t" + System.DateTime.Now.ToString("hh:mm:ss.fff") + "\t\t\tInvoking \t\"" + eventType + "\"");
#endif

            OnBroadcasting(eventType);
            Delegate d;
            if (!eventTable.TryGetValue(eventType, out d)) return;

            var callback = d as Callback<T, U>;

            if (callback == null)
            {
                throw CreateBroadcastSignatureException(eventType);
            }

            var sortDictionary = new Dictionary<int, int>();
            var index = 0;
            var invocationList = callback.GetInvocationList();
            foreach (var item in invocationList)
            {
                var it = item as Callback<T, U>;

                if (!_validate(it) || _isFilitered(it, filter))
                {
                    index++;
                    continue;
                }

                var executionAttribute =
                    it.Method.GetCustomAttribute(typeof(CExecutionOrderAttribute)) as CExecutionOrderAttribute;
                sortDictionary.Add(index, executionAttribute?.Order ?? BIG_NUMBER);
                index++;
            }

            var sortedByOrder = sortDictionary.ToList();
            sortedByOrder.Sort((firstPair, nextPair) => firstPair.Value.CompareTo(nextPair.Value));
            foreach (var order in sortedByOrder)
            {
                var value = invocationList[order.Key] as Callback<T, U>;
                if (value == null) continue;
                _logBroadCast(value, eventType);
                await value(arg1, arg2);
            }
        }

        //Three parameters
        public async void Broadcast<T, U, V>(EventType eventType, T arg1, U arg2, V arg3,
            EEventFilter filter = EEventFilter.None)
        {
#if LOG_ALL_MESSAGES || LOG_BROADCAST_MESSAGE
            //Debug.Log("MESSENGER\t" + System.DateTime.Now.ToString("hh:mm:ss.fff") + "\t\t\tInvoking \t\"" + eventType + "\"");
#endif
            OnBroadcasting(eventType);
            Delegate d;
            if (!eventTable.TryGetValue(eventType, out d)) return;

            var callback = d as Callback<T, U, V>;
            ;

            if (callback == null)
            {
                throw CreateBroadcastSignatureException(eventType);
            }

            var sortDictionary = new Dictionary<int, int>();
            var index = 0;
            var invocationList = callback.GetInvocationList();
            foreach (var item in invocationList)
            {
                var it = item as Callback<T, U, V>;
                if (!_validate(it) || _isFilitered(it, filter))
                {
                    index++;
                    continue;
                }

                var executionAttribute =
                    it.Method.GetCustomAttribute(typeof(CExecutionOrderAttribute)) as CExecutionOrderAttribute;
                sortDictionary.Add(index, executionAttribute?.Order ?? BIG_NUMBER);
                index++;
            }

            var sortedByOrder = sortDictionary.ToList();
            sortedByOrder.Sort((firstPair, nextPair) => firstPair.Value.CompareTo(nextPair.Value));
            foreach (var order in sortedByOrder)
            {
                var value = invocationList[order.Key] as Callback<T, U, V>;
                if (value == null) continue;
                _logBroadCast(value, eventType);
                await value(arg1, arg2, arg3);
            }
        }

        #endregion

        #region Helpers

        private static void _logBroadCast(Delegate del, EventType eventType)
        {
            if (del == null || del.Method.DeclaringType == null) return;
            var field = del.Method.DeclaringType.GetField("LOG_TAG",
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
            var logTag = (string)field?.GetValue(del.Target) ?? string.Empty;
            logTag = logTag == string.Empty ? del.Method.DeclaringType.Name : logTag;

            CLog.Log($"({eventType}) event raised.", category: logTag);
        }

        private static bool _isFilitered(Delegate it, EEventFilter filter)
        {
            if (it == null) return true;
            var attribute = it.Method.GetCustomAttribute(typeof(CEventFilterAttribute)) as CEventFilterAttribute;
            var attFilter = attribute?.Filter ?? EEventFilter.None;
            return !attFilter.HasFlag(filter);
        }

        private bool _validate(Delegate d)
        {
            return d != null;
            //if (d?.Target == null)
            //{
            //    return false;
            //}

            //return !(d.Target is UnityEngine.Object) || !((UnityEngine.Object)d.Target).Equals(null);
        }

        #endregion
    }

    //This manager will ensure that the messenger's eventTable will be cleaned up upon loading of a new level.
    public sealed class MessengerHelper : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        //Clean up eventTable every time a new level loads.
        public void OnLevelWasLoaded<T>(int unused, CMessenger<T> messenger)
        {
            messenger.Cleanup();
        }
    }
}