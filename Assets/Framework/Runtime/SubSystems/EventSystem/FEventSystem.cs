﻿/**
 * Author: morteza asadpour
 * CreationTime: 10 / 4 / 2017
 * Description: Base class for Event Manager
 * 
 * Edited by Ideen Molavi
 * Change base type from FComponent to FGameSystem.
 * Make class abstract.
 **/

using System;
using System.Threading.Tasks;

namespace Revy.Framework
{
    public class FEventSystem : ISubsystem, IEventSystem
    {
        private CMessenger<string> _eventMessenger;

        public Type InterfaceOfSubsystem => typeof(IEventSystem);

        #region Constructor & Destructor

        public FEventSystem()
        {
            MFramework.Register(this);
            _eventMessenger = new CMessenger<string>();
        }

        ~FEventSystem()
        {
            MFramework.UnRegister(this);
        }

        #endregion Constructor & Destructor

        void IEventSystem.ListenToEvent(string eventName, Callback callback)
        {
            _eventMessenger.AddListener(eventName, callback);
        }

        void IEventSystem.ListenToEvent<T1>(string eventName, Callback<T1> callback)
        {
            _eventMessenger.AddListener<T1>(eventName, callback);
        }

        void IEventSystem.ListenToEvent<T1, T2>(string eventName, Callback<T1, T2> callback)
        {
            _eventMessenger.AddListener<T1, T2>(eventName, callback);
        }

        void IEventSystem.ListenToEvent<T1, T2, T3>(string eventName, Callback<T1, T2, T3> callback)
        {
            _eventMessenger.AddListener<T1, T2, T3>(eventName, callback);
        }

        void IEventSystem.BroadcastEvent(string eventName, EEventFilter filter)
        {
            _eventMessenger.Broadcast(eventName, filter);
        }

        void IEventSystem.BroadcastEvent<T1>(string eventName, T1 inputValue, EEventFilter filter)
        {
            _eventMessenger.Broadcast(eventName, inputValue, filter);
        }

        void IEventSystem.BroadcastEvent<T1, T2>(string eventName, T1 inputValue1, T2 inputValue2, EEventFilter filter)
        {
            _eventMessenger.Broadcast(eventName, inputValue1, inputValue2, filter);
        }

        void IEventSystem.BroadcastEvent<T1, T2, T3>(string eventName, T1 inputValue1, T2 inputValue2, T3 inputValue3, EEventFilter filter)
        {
            _eventMessenger.Broadcast(eventName, inputValue1, inputValue2, inputValue3, filter);
        }

        void IEventSystem.RemoveEventListener<T1>(string eventName, Callback<T1> callback)
        {
            _eventMessenger.RemoveListener<T1>(eventName, callback);
        }

        void IEventSystem.RemoveEventListener<T1, T2>(string eventName, Callback<T1, T2> callback)
        {
            _eventMessenger.RemoveListener<T1, T2>(eventName, callback);
        }

        void IEventSystem.RemoveEventListener<T1, T2, T3>(string eventName, Callback<T1, T2, T3> callback)
        {
            _eventMessenger.RemoveListener<T1, T2, T3>(eventName, callback);
        }

        void IEventSystem.RemoveEventListener(string eventName, Callback callback)
        {
            _eventMessenger.RemoveListener(eventName, callback);
        }

        void IEventSystem.RemoveAllListeners(object instance)
        {
            _eventMessenger.RemoveAllListener(instance);
        }
    }
}