﻿using System;
/**
 * Author: mahdi fada
 * CreationTime: 8 / 16 / 2017
 * Description: Base class for input manager
 * 
 * Edited by ideen molavi .
 * Change base type from FComponent to FGameSystem.
 * Make this class abstract.
 **/
using UnityEngine;

namespace Revy.Framework
{
    public enum EInteractPhase
    {    //
        // Summary:
        //     ///
        //     A single interact on the screen.
        //     ///
        Began = 0,
        //
        // Summary:
        //     ///
        //     A moved interact on the screen.
        //     ///
        Moved = 1,
        //
        // Summary:
        //     ///
        //     Interact is applying and freeze in interact point.
        //     ///
        Stationary = 2,
        //
        // Summary:
        //     ///
        //     Interact is ended.
        //     ///
        Ended = 3,
        //
        // Summary:
        //     ///
        //     The system cancelled tracking for the interact.
        //     ///
        Canceled = 4
    }

    public abstract class FInputSystem<T> : IGameSystem
    {
        private static bool _isJustTouch;
        private CMessenger<T> _inputMessenger = new CMessenger<T>();

        public abstract Type InterfaceOfGameSystem { get; }

        public void ListenToInput(T messageType, Callback callEvent)
        {
            _inputMessenger.AddListener(messageType, callEvent);
        }

        public void ListenToInput<T1>(T messageType, Callback<T1> callEvent)
        {
            _inputMessenger.AddListener<T1>(messageType, callEvent);
        }

        public void ListenToInput<T1, T2>(T messageType, Callback<T1, T2> callEvent)
        {
            _inputMessenger.AddListener<T1, T2>(messageType, callEvent);
        }

        public void ListenToInput<T1, T2, T3>(T messageType, Callback<T1, T2, T3> callEvent)
        {
            _inputMessenger.AddListener<T1, T2, T3>(messageType, callEvent);
        }

        public void BroadcastInput(T messageType)
        {
            _inputMessenger.Broadcast(messageType);
        }

        public void BroadcastInput<T1>(T messageType, T1 inputValue)
        {
            _inputMessenger.Broadcast<T1>(messageType, inputValue);
        }

        public void BroadcastInput<T1, T2>(T messageType, T1 inputValue1, T2 inputValue2)
        {
            _inputMessenger.Broadcast(messageType, inputValue1, inputValue2);
        }

        public void BroadcastInput<T1, T2, T3>(T messageType, T1 inputValue1, T2 inputValue2, T3 inputValue3)
        {
            _inputMessenger.Broadcast(messageType, inputValue1, inputValue2, inputValue3);
        }

        public void RemoveInputListener(T messageType, Callback callEvent)
        {
            _inputMessenger.RemoveListener(messageType, callEvent);
        }

        public void RemoveInputListener<T1>(T messageType, Callback<T1> callEvent)
        {
            _inputMessenger.RemoveListener<T1>(messageType, callEvent);
        }

        public void RemoveInputListener<T1, T2>(T messageType, Callback<T1, T2> callEvent)
        {
            _inputMessenger.RemoveListener<T1, T2>(messageType, callEvent);
        }

        public void RemoveInputListener<T1, T2, T3>(T messageType, Callback<T1, T2, T3> callEvent)
        {
            _inputMessenger.RemoveListener<T1, T2, T3>(messageType, callEvent);
        }

        public static Vector3 GetInputVelocity()
        {
            Vector3 result = Vector3.zero;
#if UNITY_EDITOR
            if (Input.GetMouseButton(0))
            {
                result.x = Input.GetAxis("Mouse X");
                result.y = Input.GetAxis("Mouse Y");
                result *= 2;
            }
#endif
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                result = -Input.GetTouch(0).deltaPosition;
            }
            return result;
        }


        public static bool IsBackClick()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                return true;
            }
            return false;
        }

        protected void CastRay2DByInteractMouse(Camera camera, LayerMask rayMask)
        {
            Vector3 pointBuffer = Vector3.zero;
            bool isCast = false;
            pointBuffer = Input.mousePosition;
            isCast = (Input.GetMouseButtonDown(0));
            if (isCast)
            {
                CastRay(pointBuffer, camera, rayMask);
            }
        }

        protected void CastRay2DByInteractTouch(Camera camera, LayerMask rayMask)
        {
            Vector3 pointBuffer = Vector3.zero;
            bool isCast = false;
            if (Input.touchCount > 0)
            {
                TouchPhase touchPhase = Input.touches[0].phase;
                if (touchPhase == TouchPhase.Began)
                {
                    _isJustTouch = true;
                }
                else if (touchPhase == TouchPhase.Ended)
                {
                    if (_isJustTouch)
                    {
                        pointBuffer = Input.touches[0].position;
                        isCast = true;
                    }
                    _isJustTouch = false;
                }
                else if (touchPhase == TouchPhase.Moved)
                {

                    if (Input.touches[0].deltaPosition.sqrMagnitude > 16.5f)
                        _isJustTouch = false;
                }
            }
            if (isCast)
            {
                CastRay(pointBuffer, camera, rayMask);
            }
        }

        private void CastRay(Vector3 pointBuffer, Camera camera, LayerMask rayMask)
        {
            Ray ray = camera.ScreenPointToRay(pointBuffer);
            RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 10000, rayMask);
            if (hit.transform)
            {
                hit.transform.GetComponent<IInteractable>()?.OnInteract();
            }
        }

    }
}
