﻿/*
 * Description:
 * Author: Mohammad Hasan Bigdeli
 * Creation Date: 1 / 27 / 2018
 */

using Revy.Framework.Localization.Convertor;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Revy.Framework.Localization
{
    [CDisableAutoInstantiation]
    public class FLocalization : FClass, ISubsystem, ILocalization
    {
        #region Fields
        private FSLocalizationConfig _config;

        private ELanguage _currentLanguage;

        private readonly static string LOG_PREFIX = "<b><color=orange>[Localization]</color></b>";
        #endregion

        #region Properties
        public Type InterfaceOfSubsystem
        {
            get
            {
                return typeof(ILocalization);
            }
        }

        public ELanguage CurrentLanguage { get { return _currentLanguage; } }
        public Action OnLanguageChange { get; set; }
        #endregion

        #region FSubsystem Methods
        public override Task BeginPlay()
        {
            _config = null;
            //_config = ResourceManagement.LoadAsset<FSLocalizationConfig>("streaming", "LocalizationConfig");
            //if (_config)
            //    _currentLanguage = _config.DefaultLanguage;
            //else
            //    Debug.LogWarning($"{LOG_PREFIX} Can not load configuration from streaming bundles.");

            //_config?.LoadCSVFile();

            return Task.CompletedTask;
        }
        #endregion

        #region Methods
        public string GetText(string key)
        {
            if (!_config.Data.ContainsKey(key))
            {
                Debug.LogWarning($"{LOG_PREFIX} The {key} key is not exist in Localization system.");
                return null;
            }

            List<string> values = _config.Data[key];

            int currentLanguageIndex = (int)_currentLanguage;

            if (values.Count <= currentLanguageIndex)
            {
                Debug.LogWarning($"{LOG_PREFIX} The {key} key has not value in {_currentLanguage.ToString()} language.");
                return null;
            }

            if (CurrentLanguage == ELanguage.FA)
            {
                return values[currentLanguageIndex].ToPersian();
            }

            return values[currentLanguageIndex];
        }

        public FSTextSetting GetTextSetting()
        {
            return _config.Settings[(int)CurrentLanguage];
        }

        public void ChangeLanguage(ELanguage language)
        {
            _currentLanguage = language;

            OnLanguageChange?.Invoke();
        }
        #endregion
    }
}
