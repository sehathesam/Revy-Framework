﻿/*
 * Description: Attach to a UIText, set desire key in localization and it will automatically set value.
 * Author: Mohammad Hasan Bigdeli
 * CreationDate: 1 / 27 / 2018
 */

using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Revy.Framework.Localization
{
    [AddComponentMenu("UI/Localize")]
    //[RequireComponent(typeof(TextMeshProUGUI))]
    public class FLocalize : FComponent
    {
        #region Fields
        [SerializeField]
        private string _key;

        [SerializeField]
        private bool _overrideFont = true;
        [SerializeField]
        private bool _overrideFontStyle = true;
        [SerializeField]
        private bool _overrideFontSize = true;
        [SerializeField]
        private bool _overrideLineSpacing = true;
        [SerializeField]
        private bool _overrideRichText = true;
        [SerializeField]
        private bool _overrideAlignment = true;
        [SerializeField]
        private bool _overrideAlignByGeometry = true;
        [SerializeField]
        private bool _overrideHorizontalOverflow = true;
        [SerializeField]
        private bool _overrideVerticaloverflow = true;
        [SerializeField]
        private bool _overrideBestFit = true;
        [SerializeField]
        private bool _overrideColor = true;
        [SerializeField]
        private bool _overrideRaycastTarget = true;

        [SerializeField]
        private FSTextSetting _overridedSetting;

        private Text _uiText;
        #endregion

        #region Methods
        public override Task BeginPlay()
        {
            _uiText = GetComponent<Text>();

            //LocalizationManagement.OnLanguageChange += OnLanguageChagned;

            //SetText();

            return Task.CompletedTask;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            //LocalizationManagement.OnLanguageChange -= OnLanguageChagned;
        }
        #endregion

        #region Helpers
        private void OnLanguageChagned()
        {
            SetText();
        }

        private void SetText()
        {
            if (HasInitialized)
            {
                //_uiText.text = LocalizationManagement.GetText(_key);

                //if (_overridedSetting != null)
                //{
                //    ApplySetting(_overridedSetting);
                //}
                //else if (LocalizationManagement.GetTextSetting() != null)
                //{
                //    ApplySetting(LocalizationManagement.GetTextSetting());
                //}
            }
        }

        private void ApplySetting(FSTextSetting setting)
        {
            if (_overrideFont)
                _uiText.font = setting.Font;
            if (_overrideFontStyle)
                _uiText.fontStyle = setting.FontStyle;
            if (_overrideFontSize)
                _uiText.fontSize = setting.FontSize;
            if (_overrideLineSpacing)
                _uiText.lineSpacing = setting.LineSpacing;
            if (_overrideRichText)
                _uiText.supportRichText = setting.RichText;
            if (_overrideAlignment)
                _uiText.alignment = setting.Alignment;
            if (_overrideAlignByGeometry)
                _uiText.alignByGeometry = setting.AlignByGeometry;
            if (_overrideHorizontalOverflow)
                _uiText.horizontalOverflow = setting.HorizontalOverflow;
            if (_overrideVerticaloverflow)
                _uiText.verticalOverflow = setting.VerticalOverflow;
            if (_overrideBestFit)
                _uiText.resizeTextForBestFit = setting.BestFit;
            if (_overrideColor)
                _uiText.color = setting.Color;
            if (_overrideRaycastTarget)
                _uiText.raycastTarget = setting.RaycastTarget;
        }
        #endregion
    }
}
