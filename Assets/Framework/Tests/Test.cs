﻿using UnityEngine;
using Revy.Framework;
using System.Threading.Tasks;
using System;

public interface ITest
{
    void DisplayLog(string message);
}

public class Test : FComponent, ITest
{
    public void DisplayLog(string message)
    {
        Debug.Log($"Test.DispalyLog, {message}");
    }

    public override void PreInitialization()
    {
        CanInvokeTick = true; 
    }

    public override Task Initialize()
    {
        //var test = new FClassTest();
        return Task.CompletedTask;
    }

    public override void Tick()
    {
        if(Input.GetKeyUp(KeyCode.Return))
        {

            MFramework.SetPause(true);
        }

        if(Input.GetKeyUp(KeyCode.Escape))
        {
            MFramework.SetPause(false);
        }
    }

    public override void OnPause()
    {
        Debug.Log("Paused");
    }

    public override void OnResume()
    {
        Debug.Log("Resumed");
    }
}

public class TestTick : ITick, IInitializable
{
#pragma warning disable 649
    [CInject]
    private ITest _test;
#pragma warning restore 649
    //private string _message;
    public TestTick()
    {
        //_message = "";
    }

    public int ExecutionOrder => 100;

    public bool IsInitializing { set; get; }

    public bool HasInitialized { set; get; }

    public bool HasShutdown { set; get; }

    public Task BeginPlay()
    {
        _test.DisplayLog("");
        Debug.Log("BeginPlay Invoked");
        return Task.CompletedTask;
    }

    public int CompareTo(IInitializable other)
    {
        throw new NotImplementedException();
    }

    public void FixedTick()
    {
        Debug.Log("FixedTick Invoked");
    }

    public Task Initialize()
    {
        Debug.Log("Initialize Invoked");
        return Task.CompletedTask;
    }

    public void LateTick()
    {
        Debug.Log("Late Tick Invoked");
    }

    public void OnPause()
    {
        throw new NotImplementedException();
    }

    public void OnResume()
    {
        throw new NotImplementedException();
    }

    public void PreInitialization()
    {
        Debug.Log("PreInitialization Invoked");
    }

    public Task Shutdown()
    {
        return Task.CompletedTask;
    }

    public void Tick()
    {
    }
}

public class TextFTick : FComponent
{
    public override void PreInitialization()
    {
        CanInvokeTick = true;
    }

}

public class FClassTest : FClass
{
    public override void PreInitialization()
    {
        CanInvokeTick = true;
    }

    public override Task Initialize()
    {
        Debug.Log("FClassTest.Initialize() Invoked.");
        return Task.CompletedTask;
    }

    public override void Tick()
    {
        //Debug.Log("FClassTest.Tick() Invoked.");
    }

    public override Task Shutdown()
    {
        Debug.Log("FClassTest.Shutdown() Invoked.");
        return Task.CompletedTask;
    }
}